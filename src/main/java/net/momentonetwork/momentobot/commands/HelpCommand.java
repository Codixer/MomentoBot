package net.momentonetwork.momentobot.commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.entities.Message;
import net.momentonetwork.momentobot.command.CommandExecutor;

import java.awt.Color;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class HelpCommand extends CommandExecutor {

    public HelpCommand() {
        super("help");
    }

    @Override
    protected void run(Message msg, String[] args) {
        String cmds = commands.stream().map(CommandExecutor::getName).collect(Collectors.joining(",\n"));

        if (args.length == 0) {
            EmbedBuilder embed = new EmbedBuilder();
            embed.setAuthor(msg.getAuthor().getName(), null, msg.getAuthor().getAvatarUrl());
            embed.setTitle("Commands");
            embed.setDescription("\n" + cmds);
            embed.setColor(Color.BLUE);
            msg.getChannel().sendMessage(embed.build()).queue();
            return;
        }

        StringBuilder help = new StringBuilder();
        List<String> argsList = Arrays.stream(args).map(String::toLowerCase).collect(Collectors.toList());

        for (CommandExecutor ce : commands) {
            if (argsList.contains(ce.getName()))
                if (ce.getHelp() != null && !ce.getHelp().equals(""))
                    help.append(ce.getHelp()).append("\n");
        }

        EmbedBuilder embed = new EmbedBuilder();
        embed.setAuthor(msg.getAuthor().getName(), null, msg.getAuthor().getAvatarUrl());
        embed.setDescription("\n" + help.toString());
        embed.setColor(Color.BLUE);
        msg.getChannel().sendMessage(embed.build()).queue();


    }


}
