package net.momentonetwork.momentobot.commands;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.Role;
import net.momentonetwork.momentobot.command.CommandExecutor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RMRolesCommand extends CommandExecutor {

    public RMRolesCommand() {
        super("rmroles", "rmroles @role1 @role2 @role3 @etc");
    }

    @Override
    protected void run(Message msg, String[] args) {
        Guild g = msg.getGuild();

        ArrayList<String> prefixes = new ArrayList<>(Arrays.asList("Skyblock", "Dutch", "English", "Alerts"));
        ArrayList<Role> rolesToGive =
                msg.getMentionedRoles().stream()
                        .filter(r -> prefixes.contains(r.getName().split(":")[0].trim()))
                        .collect(Collectors.toCollection(ArrayList::new));

        if (rolesToGive.size() == 0){
            // todo show role help
            return;
        }
        g.getController().removeRolesFromMember(g.getMember(msg.getAuthor()), rolesToGive.toArray(new Role[rolesToGive.size()])).queue();
        msg.addReaction("✅").queue();
    }
}
