package net.momentonetwork.momentobot.command;

import net.dv8tion.jda.core.entities.Message;
import net.momentonetwork.momentobot.Main;

import java.util.ArrayList;
import java.util.Arrays;

public abstract class CommandExecutor {

    protected final static ArrayList<CommandExecutor> commands = new ArrayList<>();

    public static void addCommands(CommandExecutor... executors){
        commands.addAll(Arrays.asList(executors));
    }

    private String name;
    private String help;

    public CommandExecutor(String name) {
        this.name = name;
    }

    public CommandExecutor(String name, String help) {
        this.name = name;
        this.help = help;
    }

    public static void executeCommand(Message msg) {
        String[] splitContent = msg.getContentRaw().split(" ");
        String[] args = Arrays.asList(splitContent).subList(1, splitContent.length).toArray(new String[splitContent.length-1]);
        String name = splitContent[0].replace(Main.getSetting("cp"), "");
        commands.forEach(cmd -> { if (cmd.name.equalsIgnoreCase(name)) cmd.run(msg, args); });
    }


    protected abstract void run(Message msg, String[] args);

    public String getName() {
        return name;
    }

    public String getHelp() {
        return help;
    }
}
