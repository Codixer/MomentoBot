package net.momentonetwork.momentobot.events;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.momentonetwork.momentobot.Main;
import net.momentonetwork.momentobot.command.CommandExecutor;


public class MessageReceived extends ListenerAdapter {


    public void onMessageReceived(MessageReceivedEvent event) {
        JDA jda = event.getJDA();
        if (event.getAuthor().equals(jda.getSelfUser()))
            return;
        if (event.isFromType(ChannelType.PRIVATE)) {
            event.getChannel().sendMessage("You can't send me a private message yet.").queue();
        } else if (event.isFromType(ChannelType.TEXT)) {
            if (event.getMessage().getContentRaw().toLowerCase().startsWith(Main.getSetting("cp"))){
                CommandExecutor.executeCommand(event.getMessage());
            }
            if (event.getMessage().getChannel().getName().equals("suggestions")){
                event.getMessage().addReaction("\uD83D\uDC4D").queue();
                event.getMessage().addReaction("\uD83D\uDC4E").queue();
            }

        }
    }
}