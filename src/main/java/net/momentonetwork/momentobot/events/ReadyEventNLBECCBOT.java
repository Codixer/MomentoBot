package net.momentonetwork.momentobot.events;

import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ReadyEventNLBECCBOT extends ListenerAdapter {

    @Override
    public void onReady(ReadyEvent event) {
        super.onReady(event);
        JDA jda = event.getJDA();
        System.out.println("[NL/BE] De bot is opgestart");
        jda.getPresence().setStatus(OnlineStatus.ONLINE);
        jda.getPresence().setGame(Game.listening("lofi hip hop"));
    }
}