package net.momentonetwork.momentobot;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.momentonetwork.momentobot.command.CommandExecutor;
import net.momentonetwork.momentobot.commands.HelpCommand;
import net.momentonetwork.momentobot.commands.IpCommand;
import net.momentonetwork.momentobot.commands.RMRolesCommand;
import net.momentonetwork.momentobot.commands.RolesCommand;
import net.momentonetwork.momentobot.events.MessageReceived;
import net.momentonetwork.momentobot.events.ReadyEventNLBECCBOT;


import javax.security.auth.login.LoginException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Main {

    private static HashMap<String, String> argSettings = new HashMap<>();

    public static void main(String[] args) throws LoginException {
        for (String arg : Arrays.asList(args)){
            if (arg.contains(":")) {
                List<String> split = Arrays.asList(arg.split(":"));
                argSettings.put(split.get(0).toLowerCase(), String.join("\\:", split.subList(1, split.size())));
            }
        }

        // Note: It is important to register your ReadyListener before building
        JDA jda = new JDABuilder(AccountType.BOT)
                .setToken(Main.getSetting("t"))
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .setGame(Game.playing("Loading..."))
                .buildAsync();

        jda.addEventListener(new ReadyEventNLBECCBOT());
        jda.addEventListener(new MessageReceived());

        CommandExecutor.addCommands(new HelpCommand(), new RolesCommand(), new RMRolesCommand(), new IpCommand());

    }

    public static String getSetting(String arg){
        if (argSettings.containsKey(arg.toLowerCase())){
            return argSettings.get(arg.toLowerCase());
        }
        throw new NullPointerException("Missing startup arg '" + arg + "'");
    }


}
